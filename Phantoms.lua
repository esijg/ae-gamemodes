-- Phantoms.lua
-- Invisibility Multiplayer Gamemode
-- By Jóhannes Gunnar Þorsteinsson https://www.johannesg.com

Triggers = {}

light_reduction = 10
visibility_time = 30

function player_invisibility()
    for p in Players() do
        if (p.action_flags.left_trigger == true or p.action_flags.right_trigger == true ) then
            p.invisibility_duration = 0
            p._trigger_time = 0
        elseif (p._trigger_time > visibility_time or Game.ticks == 0) then
            p.invisibility_duration = 999
        end
        p._trigger_time = p._trigger_time + 1
    end
end

function lights_off()
    for a in Polygons() do
        if (a.floor.light.index <= 20-light_reduction) then
            a.floor.light = a.floor.light.index + light_reduction
        elseif (a.floor.light.index > 20-light_reduction and a.floor.light.index <= 20) then
            a.floor.light = 20
        else
            a.floor.light = 20
        end
        if (a.ceiling.light.index <= 20-light_reduction) then
            a.ceiling.light = a.ceiling.light.index + light_reduction
        elseif (a.ceiling.light.index > 20-light_reduction and a.ceiling.light.index <= 20) then
            a.ceiling.light = 20
        else
            a.ceiling.light = 20
        end
    end
    for s in Sides() do
        if (s.primary.light.index <= 20-light_reduction) then
            s.primary.light = s.primary.light.index + light_reduction
        elseif (s.primary.light.index > 20-light_reduction and s.primary.light.index <= 20) then
            s.primary.light = 20
        else
            s.primary.light = 20
        end
        if (s.secondary.light.index <= 20-light_reduction) then
            s.secondary.light = s.secondary.light.index + light_reduction
        elseif (s.secondary.light.index > 20-light_reduction and s.secondary.light.index <= 20) then
            s.secondary.light = 20
        else
            s.secondary.light = 20
        end
        if (s.transparent.light.index <= 20-light_reduction) then
            s.transparent.light = s.transparent.light.index + light_reduction
        elseif (s.transparent.light.index > 20-light_reduction and s.transparent.light.index <= 20) then
            s.transparent.light = 20
        else
            s.transparent.light = 20
        end
    end
end

function Triggers.init()
    for p in Players() do
        p._trigger_time = 0
    end
end

function Triggers.idle()
    -- Setup
    if(Game.ticks == 0) then
        lights_off()
    end
    player_invisibility()
end
